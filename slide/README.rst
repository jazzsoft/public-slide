******
README
******

여기에는 공개 가능한 darkslide의 예제를 올려봅니다. Slide를 보기위해서는 darkslide를 설치하셔야 합니다. 아래 링크를 참고하십시오.

`Darkslide Tutorial <https://github.com/ionelmc/python-darkslide>`_

rst화일에 대한 자료는 아래 링크를 참고하십시오.

`reStructuredText <https://docutils.sourceforge.io/rst.html>`_

